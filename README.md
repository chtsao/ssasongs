SSA Songs and Link

## 選歌單 2019
### 福爾摩莎合唱團/蔡昱姍
1. 龜笑鱉無尾/福爾摩沙系列
2. [一樣的炒米香/福爾摩沙系列](https://youtu.be/6-59W6wriGM)
3. [台灣是寶島/福爾摩沙系列/蕭泰然](https://youtu.be/kTFAEe6lOgs)
4. [心靈的禱告/福爾摩沙系列/蕭泰然](https://youtu.be/CScNLHZMYd0)
5. [阿母的頭鬘/福爾摩沙系列/蕭泰然](https://youtu.be/OImUf-taMXQ)
6. [十八姑娘/福爾摩沙系列/謝宇威](https://youtu.be/5QnR7LCNJCw)
7. [青山綠水好風光/福爾摩沙系列/長潭山歌](https://youtu.be/6pjNlZ4GQgg)
8. [月光光/蔡昱姍/客家民謠](https://youtu.be/_nG64SiPfh8)
9. [頭擺的妳/蔡昱姍/陳永淘](https://youtu.be/1DOwUKuNZSk?t=301)
10. [孤戀花/蔡昱姍/楊三郎](https://youtu.be/81bVMpxRQjQ)
11. [思慕的人/蔡昱姍/洪一峰](https://youtu.be/VBQOlvynZhs)

###  周鑫泉
1. [我記得(西風的話)/周鑫泉](https://youtu.be/uxevRUDGuwA)
2. [去罷/周鑫泉/徐志摩](https://youtu.be/u0V5SGK2-Yk)
### 
1. [媽媽的花環/台灣合唱音樂中心/陳建年](https://youtu.be/RmWYakWJ3NE). Youtube 有不同Key的版本。
2. [幸運草/音契文化/林京美](https://youtu.be/EMk1r3JAJ08)
3. [愛如風吹/基督教救世傳播會/鄭金川](https://youtu.be/XiEtMrK5cDk). 台語聖歌。獨唱（流行版）
